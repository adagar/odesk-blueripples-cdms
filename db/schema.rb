# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120813092203) do

  create_table "branches", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "address"
    t.string   "phone"
    t.string   "alternate"
    t.string   "pricing"
    t.boolean  "online"
    t.boolean  "active"
    t.date     "operationstartdate"
    t.text     "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "branch_id"
  end

  create_table "employees", :force => true do |t|
    t.string   "employeeid"
    t.string   "title"
    t.string   "fore_name"
    t.string   "sur_name"
    t.string   "known_as"
    t.date     "date_of_birth"
    t.string   "sex"
    t.string   "phone"
    t.string   "mobile_number"
    t.string   "job_title"
    t.string   "address_line_one"
    t.string   "address_line_two"
    t.string   "address_line_three"
    t.string   "address_line_four"
    t.string   "postcode"
    t.string   "email_address"
    t.date     "date_of_joining"
    t.string   "comments"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", :force => true do |t|
    t.string   "productid"
    t.string   "name"
    t.text     "description"
    t.string   "unit_price"
    t.string   "universal_product_code"
    t.integer  "available_quantity"
    t.string   "status"
    t.string   "warning_quantity_level"
    t.string   "low_quantity_level"
    t.string   "priority"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "treatments", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "time_period"
    t.string   "status"
    t.string   "pricing"
    t.string   "base_time"
    t.string   "price"
    t.string   "priority"
    t.boolean  "taxable"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "treatment_id"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_id"
    t.string   "sercret_question"
    t.string   "secret_answer"
    t.string   "user_type"
    t.string   "user_status"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
