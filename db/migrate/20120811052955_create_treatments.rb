class CreateTreatments < ActiveRecord::Migration
  def change
    create_table :treatments do |t|
      t.string :name
      t.text :description
      t.string :time_period
      t.string :status
      t.string :pricing
      t.string :base_time
      t.string :price
      t.string :priority
      t.boolean :taxable

      t.timestamps
    end
  end
end
