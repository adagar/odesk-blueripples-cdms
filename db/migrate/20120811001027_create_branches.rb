class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.string :name
      t.text :description
      t.text :address
      t.string :phone
      t.string :alternate
      t.string :pricing
      t.boolean :online
      t.boolean :active
      t.date :operationstartdate
      t.text :comments

      t.timestamps
    end
  end
end
