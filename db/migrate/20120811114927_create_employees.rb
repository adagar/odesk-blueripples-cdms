class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :employeeid
      t.string :title
      t.string :fore_name
      t.string :sur_name
      t.string :known_as
      t.date :date_of_birth
      t.string :sex
      t.string :phone
      t.string :mobile_number
      t.string :job_title
      t.string :address_line_one
      t.string :address_line_two
      t.string :address_line_three
      t.string :address_line_four
      t.string :postcode
      t.string :email_address
      t.date :date_of_joining
      t.string :comments
      t.string :status

      t.timestamps
    end
  end
end
