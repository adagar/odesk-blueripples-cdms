class AddTreatmentIdToTreatments < ActiveRecord::Migration
  def change
    add_column :treatments, :treatment_id, :string
  end
end
