class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :productid
      t.string :name
      t.text :description
      t.string :unit_price
      t.string :universal_product_code
      t.integer :available_quantity
      t.string :status
      t.string :warning_quantity_level
      t.string :low_quantity_level
      t.string :priority

      t.timestamps
    end
  end
end
