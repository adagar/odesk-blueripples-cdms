class ApplicationController < ActionController::Base
  protect_from_forgery
  rescue_from CanCan::AccessDenied do |exception|
    flash[:notice] = "Unauthorized Access!!"
    redirect_to root_url, :alert => exception.message
  end
end
