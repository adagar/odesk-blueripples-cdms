class Product < ActiveRecord::Base

	validates :productid, :presence => { :message => " can not be left blank" }, :uniqueness => { :message => " must be unique" }

	validates :name, :presence => { :message => " can not be left blank" }

	validates :unit_price, :presence => { :message => " can not be left blank" }, :numericality => { :message => " must be a number"}

	validates :universal_product_code, :presence => { :message => " can not be left blank" }, :length => { :is => 10, :message => " must be 10 characters long" }

	validates :status, :presence => { :message => " can not be left blank" } 	

  # Setup accessible (or protected) attributes for your model
attr_accessible :productid, :name, :description, :unit_price, :universal_product_code, :available_quantity, :status, :phone, :warning_quantity_level, :low_quantity_level, :priority

end
		
