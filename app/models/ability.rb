class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
       user ||= User.new # guest user (not logged in)
    #   if user.role?()
    #     can :manage, :all
    #   elsif
    #   can :read, :all
#	if  user.user_type?(:"HO Adminstrator") || user.user_type?(:Master)
#  	  can :read, Employee
# 	  can :create, Employee
#     	  can :update, Employee 
#       elsif user.user_type?(:"HO Adminstrator") || user.user_type?(:Master)
#  	  can :read, Bramch
# 	  can :create, Branch
#     	  can :update, Branch 
#       elsif user.user_type?(:"HO Adminstrator") || user.user_type?(:Master)
#  	  can :read, Treatment
# 	  can :create, Treatment
#     	  can :update, Treatment 
#       elsif user.user_type?(:"HO Adminstrator") || user.user_type?(:Master)
#  	  can :read, Product
# 	  can :create, Product
#     	  can :update, Product 
#       end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities


   #adding other features as per requirement here
   end
end
