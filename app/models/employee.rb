class Employee < ActiveRecord::Base

	validates :employeeid, :presence => { :message => " can not be left blank" }, :uniqueness => { :message => " must be unique" }
        
	validates :title, :presence => { :message => " can not be left blank" } 
	
	validates :fore_name, :presence => { :message => " can not be left blank" }, :length => { :in => 2..40, :message => " must be between 2 to 40 charachters long" }

	validates :sur_name, :presence => { :message => " can not be left blank" }, :length => { :in => 2..40, :message => " must be between 2 to 40 charachters long" }

	validates :known_as, :presence => { :message => " can not be left blank" }, :length => { :in => 2..40, :message => " must be between 2 to 40 charachters long" }

	validates :date_of_birth, :presence => { :message => " please select the date of birth" } 

	validates :sex, :presence => { :message => ": please select gender" } 	

	validates :job_title, :presence => { :message => " can not be left blank" } 	

	validates :postcode, :presence => { :message => " can not be left blank" }, :length => { :is => 6, :message => " must be 6 characters long" }

	validates :date_of_joining, :presence => { :message => " can not be left blank" } 	

	validates :status, :presence => { :message => " can not be left blank" } 	

  # Setup accessible (or protected) attributes for your model
attr_accessible :employeeid, :title, :fore_name, :sur_name, :known_as, :date_of_birth, :sex, :phone, :mobile_number, :job_title, :address_line_one, :address_line_two, :address_line_three, :address_line_four, :postcode, :email_address, :date_of_joining, :comments, :status

end
