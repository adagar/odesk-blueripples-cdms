class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

	validates :user_id, :presence => { :message => " ID can't be left blank" }, :uniqueness => { :message => " must be unique" }

	validates :user_type, :presence => { :message => " can't be left blank" } 
	
	validates :user_status, :presence => { :message => " can't be left blank" } 	

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :sercret_question, :user_id, :secret_answer, :user_type, :user_status
end
