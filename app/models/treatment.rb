class Treatment < ActiveRecord::Base

	validates :treatment_id, :presence => { :message => " ID can not be left blank" }, :uniqueness => { :message => " must be unique" }

	validates :name, :presence => { :message => " can not be left blank" } 

	validates :description, :presence => { :message => " can not be left blank" } 

	validates :time_period, :presence => { :message => " can not be left blank" } 

	validates :status, :presence => { :message => " can not be left blank" } 

	validates :pricing, :presence => { :message => " can not be left blank" } 

	validates :price, :presence => { :message => " can not be left blank" } 

   # Setup accessible (or protected) attributes for your model
attr_accessible :treatment_id, :name, :description, :time_period, :status, :pricing, :base_time, :price, :priority, :taxable


end
